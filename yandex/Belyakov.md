# Yandex


# html

`<h3 class="content-block__tittle--h3">Капсула будет полезна тем, кто:</h3>` - модификаторы без блока или элемента не используем. Только так: `<h3 class="content-block__tittle content-block__tittle--h3">Капсула будет полезна тем, кто:</h3>`

Элементы без блока не используем
```html
<ul class="content-block__list">
  <li class="list__item">плохо себя чувствует и работает менее эффективно из-за постоянного недосыпа;</li>
```
Нужно `<ul class="content-block__list list">`

Аналогично
```html
<div class="card-block card-block--questions"> <!-- Нужно добавить класс блока block-questions -->
  <div class="block-questions__content">
```

# css

```css
.nav.is-active {

    background-color: var(--theme-accent-text-2);
    height: 280px;
    position: absolute;
    top: 43px;
    transition: 1s;
    /* Твои стили из-за которых появился горизонтальный скролл на ipad */
    width: 120%;
    right: -25px;
    
    /* Как будет лучше */
    width: 100vw;
    left: 50%;
    transform: translateX(-50%);
}
```
По стилям хорошо)

# js

[Как достать элемент](https://www.dropbox.com/scl/fi/o9lgbks1936ngmvnhlh2i/%D0%91%D1%80%D0%B0%D1%83%D0%B7%D0%B5%D1%80%D0%BD%D1%8B%D0%B9-JS.paper?dl=0&rlkey=kdjjd98rqcuvatd38hawwxksw) и почему нужно отдать предпочтение `getElementsByClassName`.

`let elem = document.querySelector('nav');` - querySelector, да плюс каждый раз при клике ты заново ищешь `elem`.
```javascript
const elem = document.getElementsByClassName('js-nav'); // выносим за пределы burgers.forEach(...)
```

Google map, вот [тут](https://www.dropbox.com/scl/fi/98knbhn7d7qewacrfag1q/_.paper?dl=0&rlkey=764zv3alaygzbtrecf7k469ta#:uid=337947443264822777334326&h2=%D0%9A%D0%B0%D1%80%D1%82%D0%B0-Google-Maps) подробная инструкция. Но... подключил + маркер есть  - ок)

К аккордеонам не добрался(


