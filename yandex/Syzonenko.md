# Yandex

Контейнер попал на кнопку, из-за этого появился горизонтальный скролл
```html
<div class="button-wrapper container">
  <a class="button" href="">Забронировать</a>
</div>
```
# html
Хорошо)

# css
Отлично)

# js

Но почему `querySelector`?!

Лучше избегать коллизии в названиях переменных, `burgers.forEach`
```javascript
burger.forEach(function (burger) {
	burger.addEventListener('click', (e) => {
```
Префиксы `js-` - `faq__item`, `faq__answer`

У forEach в колбек можно передать доп аргумент. Посмотреть [тут](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach)
```javascript
questions.forEach(function (question, index ) {
    // let ind = questions.indexOf(e.target); - не нужно
})
```

переключатель отзывов - в принципе, можно для практики в js, в реальности, нужно подключить [слайдер](https://www.dropbox.com/scl/fi/98knbhn7d7qewacrfag1q/-%D0%9A%D0%B0%D0%BA-%D0%BF%D0%BE%D0%B4%D0%BA%D0%BB%D1%8E%D1%87%D0%B8%D1%82%D1%8C-%D1%81%D0%BB%D0%B0%D0%B9%D0%B4%D0%B5%D1%80-%D0%B8-%D0%BA%D0%B0%D1%80%D1%82%D1%83.paper?dl=0&rlkey=764zv3alaygzbtrecf7k469ta), т.к. он: готов, легко настраиваемый, легко адаптируется. А на "написать свой слейдер", а еще и универсальный уйдет ооочень много времени