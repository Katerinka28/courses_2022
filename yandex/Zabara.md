# Yandex

Секцию с картой можно было достать с контейнера) на всю ширину смотрится лучше)

Попробуй подключить `<picture>` `<source>` [srcset](https://developer.mozilla.org/ru/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images#%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5_%D1%80%D0%B0%D0%B7%D1%80%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D1%8F_%D1%80%D0%B0%D0%B7%D0%BD%D1%8B%D0%B5_%D1%80%D0%B0%D0%B7%D0%BC%D0%B5%D1%80%D1%8B)
для главной картинки)


# html

Сорри, нужно было сразу пример привести)
```html
<div class="inside__cont inside__grid-1">... 
Должно быть так
<div class="inside__cont inside__cont--grid-1">
```
Элементы не используем в отрыве от блоков
```html
<div class="inside__item"><span class="pod__span"></span>Ортопедический матрас</span></div>
```

Не нужно дублировать одно и тоже с увеличением расширения экрана http://joxi.ru/bmoNqDdUO3JJ9r

# css

Лучше было бы `position: absolute;`  сделать, а еще лучше, чтобы это был тег `li` и использовать `list-style-image: url(../img/icons/Rectangle3505.svg)`
```css
.pod__item::before {
  z-index: 1;
  position: relative;
  width: 14px;
  height: 14px;
  left: -18px;
  top: 0;
  content: url(../img/icons/Rectangle3505.svg);
}
```
0px
```
padding: 0px 15px;
margin: 0px auto;
```
`margin: 0 5px 0 0;` => `margin-right: 5px`
```css
.is-active {
  transform: rotate(45deg);
}
```
21 раз повторяется 
```css
  font-style: normal;
  font-weight: 400;
```


# js
Тут все-таки, что-то не задалось. Где префиксы?(
```javascript
document.getElementsByClassName("burger__item")
document.getElementsByClassName("accordion__block")
$(".variable").slick({...})
```


