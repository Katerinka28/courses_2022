# Yandex

Аккордеончики на чекбоксах - молодец)
Желательно карту подключить другим способом)

# html
Отлично)

И тут силы семантики тебя покинули( div(можно сделать еще p-ку), div(footer?)
```html
<div class="tab__content">
  400Р и спишь довольный
</div>
<div class="footer">
  © 2022, Яндекс.Практикум
</div>
```
БЭМ - хорошо) ниже есть небольшое замечание)

# css
Нужно больше ccs-переменных)) все цвета) можно font-size-ы тоже)

Может модификаторы?!))
```css
.inside-capsule__item.item-1 { -> .inside-capsule__item--1
  order: 1;
}
.inside-capsule__item.item-4 { -> .inside-capsule__item--4
  order: 6;
}
.inside-capsule__item.item-3 { -> .inside-capsule__item--3
  order: 4;
}
.inside-capsule__item.item-6 { -> .inside-capsule__item--6
  order: 2;
}
.inside-capsule__item.item-5 { -> .inside-capsule__item--5
  order: 5;
}
.inside-capsule__item.item-2 { -> .inside-capsule__item--2
  order: 1;
}
```
Нулевым замечание не нужны единицы измерения. Можно назначить точено
```css
.menu__item {
  margin: 0px 20px 0px 0px; /* margin: 0 20px 0 0 */ /* margin-right: 20px */
}
```

`@media (max-width: 992px) {}` desktop-first сводим к минимуму 

Зачем тут 409px 
```css
@media (min-width: 768px)
.questions {
    margin: 115px 0 409px 7px;
}
```

В целом хорошо)

# js

Читаем тут почему [getElementsByClassName](https://www.dropbox.com/scl/fi/o9lgbks1936ngmvnhlh2i/JS.paper?dl=0&rlkey=kdjjd98rqcuvatd38hawwxksw#:uid=672203874793407432828702&h2=%D0%9A%D0%B0%D0%BA-%D0%B4%D0%BE%D1%81%D1%82%D0%B0%D1%82%D1%8C-%D1%8D%D0%BB%D0%B5%D0%BC%D0%B5%D0%BD%D1%82) лучше)

Там же про перфиксы `.js-` для javascript

А [тут](https://www.dropbox.com/scl/fi/98knbhn7d7qewacrfag1q/_.paper?dl=0&rlkey=764zv3alaygzbtrecf7k469ta) про карту