# Trevland

Честно, ScrollReveal плохо сочетается с не оконченной версткой(

http://joxi.ru/v29wx0Dt43YXor - картинка съехала, бордер сам по себе
http://joxi.ru/l2Zj57MHlwNyw2 - не законченная форма подписки 

# html

В целом хорошо, можно немного сократить количество классов.

`<a href="#" class="nav__button button">Booking now</a>` - nav__button не используется в стилях

# css

css-переменные вроде и есть, но... все равно в коде `color: #000000;`, нужно использовать даже для таких базовых цветов. Для всех короче)

Данные значение по умолчанию, их дублировать не нужно
```css
font-style: normal;
font-weight: normal;
```
Почему же `.menu__body` такое маленькое?! А вот почему
```css 
.menu__body {
  height: 100%;
}
```
100% от первого `position: relative;` родителя, т.е. хедера. И, естественно, `top: -100%;` работает аналогично.

Самой простой способ вылечить
```css 
.menu__body {
  ...
  max-height: 100%;
  ...
}
.menu__body.active {
  top: 0;
  max-height: unset;
}
```
По стилям - неплохо) но ты забыл о css-переменных вовсе, даже для media-query
```css
@media (min-width: 1200px) {
  .container {
    width: 1200px;
  }  
}
```

# js

Префиксы  `js-` для javascript-а.

В html навешиваем класс `js-menu-icon`, в css не используем его, этот класс живет только для js-a, и потом
```javascript 
const iconMenu = document.querySelector(".menu__icon");
```
для тогла стилизации все остаётся как есть)

Достаточно было добавить стили
```javascript
document.body.classList.toggle("disabled");
```
```css
body.disabled {
  overflow: hidden;
}
```

`scrollHeader` - заработало, если были бы стили

