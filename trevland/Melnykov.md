# Trevland

Мобильное меню обделил стилями, прозрачное  с белым цветом шрифта(

# html

Отлично)

# css

Хорошо)

Можно было не изменять background-position. Оставить `background-position: center;` для всех медиа. 
```css
.railtrips{
	background-image: url("../img/railtrips/background.png");
	background-size: cover;
	background-position-x: center;
	background-position-y: center;
}
```
Представь, появляется `is-active` на `body`, что будет с бургером?! Верно! Будет крестик!
```css
.is-active .burger__item {
  transform: rotate(90deg);
}
```
Нужно использовать комбинированный селектор `.burger.is-active .burger__item`. Тогда бургер будет крестиком, только `is-active` будет на `burger`.

# js

Хорошо) но...

Читаем [тут](https://www.dropbox.com/scl/fi/o9lgbks1936ngmvnhlh2i/%D0%91%D1%80%D0%B0%D1%83%D0%B7%D0%B5%D1%80%D0%BD%D1%8B%D0%B9-JS.paper?dl=0&rlkey=kdjjd98rqcuvatd38hawwxksw) почему не стоит использовать `getElementById`

На всякий случай ознакомится почему лучше использовать [`addEventLister`](https://learn.javascript.ru/introduction-browser-events#addeventlistener) .
