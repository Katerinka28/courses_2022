# Trevland

Красиво) Шрифты понравились)
`+` за git)

# html

Отлично)

# css

`.overlay` - желательно, чтобы картинка не растягивалась, изменить `background-size: 100% 82%;` на `background-size: cover;`

css-переменные 
```css
.nav-mobile__link:hover {
  text-shadow: 3px 3px 10px red, -3px -3px 10px #ff0;
}
```

Лучше держать media-query в одном месте) данный код живет отдельно(
```css
@media screen and (min-width:480px) {
  .hero {
    padding-bottom: 150px;
  }
}
```
Сохраняй каскадность стилей, сначала пишем общие стили для селекторов, потом уже раздельные
```css
.burger::before {
  top: 0;
}

.burger::after {
  bottom: 0;
}

.burger::before,
.burger::after {
  content: '';
  height: 2px;
  width: 100%;
  position: absolute;
  background-color: var(--secondary-text-color);
}
```
▼
```css
.burger::before,
.burger::after {}
.burger::before {}
.burger::after {}
```

Желательно стили модификаторов держать рядом с блоками-элементами, так сказать, группировать 

274 строка `.description__title`
304 строка `.description__title--blog`




# js

Не нужно передавать и принимать параметры функциям)

Замыкания, если осмыслено, то `+` за использование, но можно и без него обойтись(только усложняет), но, скорей всего, это копипаст

menu.js - и тут что-то пошло не так:
```javascript
const clickMenu = (e) => {
  if ((menu.style.display = "none")) { // присвоение вместо сравнения
    menu.style.display = ""; // лечим то, что сломали строкой выше
    e.currentTarget.addEventListener("click", openMenu); // Добавляем и через строку удаляем event listener
    e.currentTarget.addEventListener("click", closeMenu); // аналогично добавляем и через строку удаляем event listener
    e.currentTarget.removeEventListener("click", openMenu); 
    e.currentTarget.removeEventListener("click", closeMenu);
    e.currentTarget.addEventListener("click", toggleMenu); // Работает только это
  }
};
```
`querySelectorAll` - по классу :-( 
```javascript
const links = document.querySelectorAll(".nav-mobile__link");

links.forEach((link) => {
  link.addEventListener("click", (e) => {
    e.preventDefault();
    menu.style.display = "none"; // избегаем изменения стилей с помощью js-а, тем более строкой ниже делается то же самое
    // Две строки ниже - это твоя функция closeMenu
    menu.classList.remove("is-open"); 
    burgerBtn.classList.remove("is-open");
    burgerBtn.addEventListener("click", clickMenu); // Добавление еще одного event listener-а тут излишне.
  });
});
```
Итого, если почистить menu.js, выйдет 26 строк)). Стрелочные функции `+`

Еще бы на `body` `overflow: hidden` навесить при отрытом бургере, запретить скролл)
