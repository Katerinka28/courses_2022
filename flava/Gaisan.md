# Flava

Отлично)

Можно избежать хардокных padding-ов
```sass
.to-up {
  ...
  padding-top: 20px;
  padding-left: 8px;
}
.to-up {
  ...
  display: flex;
  align-items: center;
  justify-content: center;
}
```
Перестаралась с копипастом `@media (min-width: 1200px)` везде еще раз задавала, достаточно одного раза
```sass
.container {
  --container-width: var(--mq-size-xl);
}
```
Горизонтальный скролл лечим
```sass
.video-container__image {
  max-width: 100%
}
```


main.js - лучше разбивать скрипты на модули(файлы) и потом импортировать в main) (slider.js , validation.js , burger.js , etc)

Нужно было добавить префикс js
```javascript
let daysSpan = clock.getElementsByClassName('days')[0];
let hoursSpan = clock.getElementsByClassName('hours')[0];
let minutesSpan = clock.getElementsByClassName('minutes')[0];
let secondsSpan = clock.getElementsByClassName('seconds')[0];
```

Не изменяем стили, навешиваем классы)
```javascript
if (window.pageYOffset > 300) {
    toTopBtn.style.display = 'block'
} else {
    toTopBtn.style.display = 'none'
}
```

`document.addEventListener('DOMContentLoaded', () => {})` - лучше иметь один такой прослушивать и там вызывать функции

`window.scrollBy` - заменим window.scrollTo({ top: 0, behavior: "smooth" }) и  без вычислений))
