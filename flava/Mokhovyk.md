

# Flava

Мобилка-планшет - хромает(

`<main>` - потерял)

Модификаторы без блока(
```html
<div id="scrollTop" class="show--scrollTop">`
<h1 class="header__logo--text">Flava</h1>
<h4 class="section-third__wrapper--time" id="day"></h4>

```
`section-first`, `section-first`, `section-third` - не информативные имена классов(

Sass-переменны `+`) но не забываем о css-переменных

Напутано: 
```sass
#scrollTop
	position: fixed
	bottom: 50px
	right: 70px
	font-size: 40px
	z-index: 3
	transition: .65s

.show--scrollTop
	opacity: 0

.show--scrollTop__hide
	opacity: 1
```
1. Не стилизуем ничего кроме классов. 
2. Изначальное состояние `opacity: 0` - `.scrollTop--active или .scrollTop.is-active opacity: 1`.
3. Сделал кнопку прозрачное, но она все равно на том же месте, и на нее можно кликнуть



Префикс `js-`?
```javascript
const btn = document.getElementById('btntoogle');
const frame = document.getElementById('frame');
const img = document.getElementById('img');
```

А что с валидацией не получилось? Почему не использовал `Validator`?
`const validator = new Validator(form, async function (err, is_valid) {...}`