
# Flava

Картину `main-screen__img` для desktop-а нужно было подменить, можно было через [`picture source`](https://developer.mozilla.org/ru/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images)

`object-fit: cover` - спасет от искажений изображений
```css
.main-screen__img {
  object-fit: cover;
}
```

http://joxi.ru/GrqqbQKhzGJNar - как тебе вообще удалось сохранить картинки с такими названиями?!))

Jquery, js slick-carousel и стили slick-carousel можно было импортировать  в соответствующие файлы (в main.js и style.sass)

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
```
main.js
```javascript
import slick from 'slick-carousel';
import $ from 'jquery';
```
style.sass
```sass
@import 'node_modules/slick-carousel/slick/slick.scss'

```
В этом плане нужно было разобраться с сборщиком тоже)

Верста хромает)

