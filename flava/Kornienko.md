# Flava

По коду - хорошо.

Нужно было положить `my.js` в client/src/js и импортировать его в `main.js`.

А еще лучше разбить my.js на модули(файлы).

Собственно для этого и сбощик, чтобы не писать НИЧЕГО с `server/markup/`. Ни шаблоны, ни js, ни css(убрать destyle.)! Об этом не раз упоминалось!!!

Код, который идет из клиента прогоняется через доп фичи. К примеру его можно минимизировать, доставить вендорные префиксы, сделать полифилы(поддержку новых фичей не новыми браузерами или теми, кто еще не поддерживает)


Html писать в js -- плохая практика. Ищем в доме - ими оперируем. А не строим html из скрипта

`document.addEventListener('DOMContentLoaded', () => { ` желательно когда один на сайте. так мы следим за постепенной зап=грузкой и выполнением скриптов. А не скопом все кидаем в стек вызовов.

Сборщик `-`

Наверное не заметил, но мобильное меню потянуло страницу(горизонтальный скролл) http://joxi.ru/KAxQvZXUVKYByr

Зачем два раза искать в доме элементы, можно записать в одну переменную, а потом уже определить твои переменные 
```javascript
let video = Array.from(document.getElementsByClassName('js-play'))[0];
const play = Array.from(document.getElementsByClassName('js-play'))[1];
//  хотя бы так
const videoPlaysElement = Array.from(document.getElementsByClassName('js-play'))
let video = videoPlaysElement[0]
const play = videoPlaysElement[1]
```
