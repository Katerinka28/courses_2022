# Belyakov Instagram

Структура проекта - лучше добавить несколько папочёк (css, img).

Верстка поехала в разные стороны http://joxi.ru/DmBdQV4UgXga7A ( О табличной верстке забываем, это прожитое прошлое) flex, grid - настоящее)

Семантика - обилие тегов радует, но недостаточно:
  - `nav` -> `ul` -> `li` - цепочка для отображения меню;
  - `span` - строчный элемент, он априори не может в себе содержать блочные элементы (`div`,`h1`);
  - `<input type="image" src="...">` - прям что-то новенькое, но не стоит, сделай обычной картинкой `img`;
  - некоторые элементы должны быть ссылками (followers, stories).


БЭМ - вроде начал хорошо, но, потом что-то пошло не так, и существуют только элементы несуществующих блоков

Разберём хедер, например, `header` блок - отлично.
```html 
<span class="header__menu">
    <a href="" class="menu__button">
      <input type="image" src="inst-home.png" alt="inst-home">
      <div class="messenger__messages">5</div>    
```
Лучше так
```html
<nav class="header__menu">
    <ul class="menu">
      <li class="menu__item">
        <a href="" class="menu__link">
          <img src="inst-home.png" class="menu__img">
        <span class="menu__count">5</div>
```
Иногда можно сократить
```html
<nav class="header__menu menu">
  <a href="" class="menu__link">
    <img src="inst-home.png" class="menu__img">
    <span class="menu__count">5</div>
```
Представь, тебе нужно будет разместить похожее меню в footer-е, `.header__menu` - отвечает за расположение в контексте header-а (margin-ы например), `.menu` - отвечает за внутренние стили, к примеру `display: flex;`




## CSS

Не вижу css-переменных(

Шрифты ты подключаешь с одним именем, а используешь с другим(
```css
@font-face {
	font-family: 'HelveticaNeueCyr';
}
.some-class {
  font-family: Helvetica Neue;
}
```

С header-ом не понятно твое решение, зачем абсолютное позиционирование?!
```css
.header {
    position: absolute;
    left: 0;
    top: 0;
    z-index: 50;
}
```
Если ты хотел приклеить header, то нужно `position: fixed;`, в данном случаи можно еще `position: sticky`, помнишь мы об этом говорили)

Ховер-эффекты не везде и им не хватает `transition: 0.5s` , к примеру) Потому что ховеры грубоватые получились

