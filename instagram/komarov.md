
# Komarov Instagram

Порадовал) Свои картинки +, адаптив +, лоадер)) 

Семантика - маловасто(
```html
<div class="header"> <!-- <header class="header"> -->
<div class="main">  <!-- <main class="main"> -->
<div href="#" class="cards__link"> <!-- вот тут опечатался -->
```

Ну и где же иконки?! 
```html
<i class="fas fa-heart" aria-hidden="true">
```

БЭМ - в целом, хорошо, не забываем о модификаторах

`.block__item`,`.item__block`, `.other__link` - элементы без блока не могут существовать
```html
<div class="about__block">
  <div class="block__item">
      <h1 class="about__nickname">J_Young</h1>
      <span class="item__block">
        <a href="#" class="item__link">
          <img src="assets/img/Ellipse.svg" alt="" class="other__link">
```

Ну и карточка, карточка - должна быть отдельным блоком. Карточки на 99% переиспользуемый элемент
```html
  <div class="cards">
    <div class="cards__item">
      <a href="#" class="card">
        <img src="assets/img/cards/card1.jpg" alt="card1" class="card__img">
        <div class="card__info">
          <ul>
            <li class="card__likes"><span class="visually__hidden">Likes:</span><i class="fas fa-heart"
                aria-hidden="true"></i> 1477</li>
            <li class="card__comments"><span class="visually__hidden">Comments:</span><i
                class="fas fa-comment" aria-hidden="true"></i> 545</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
```

## CSS

Но где же css-переменный с цветами(

Шрифты должны лежать в проекте(


`.actual__img` - не хватает `object-fit: cover;`

`.buttons p` - ну... нужно же через класс, не стилизуем теги!!!

Не хватает ховер-эффектов(

По поводу лайков и комментов - хорошо придумал)
Можно этому блоку абсолютное позиционирование задать, и дальше красивенько стилизовать)
```css
.cards__link {
  position: relative;
}
.cards__link-info {
  position: absolute;
}
```
Так делать не стоит, если текста больше будет... 
```css 
.information{
    position: absolute;
}
```

В целом - хорошо)
