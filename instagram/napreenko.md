
# Napreenko Instagram

Верстка немного съехала) наверное рука дрогнула http://joxi.ru/krDK6P4ug1d6XA
```css
.wrapper{
	max-width: 2247px; => max-width: 1247px;
}
```
Семантика - отлично)

БЭМ - тоже хорошо)


И самая распространенная ошибка - карточка должна быть отдельным блоком. Карточки на 99% переиспользуемый элемент

http://joxi.ru/V2VMNxpt81Bgem

```html
<div class="my-post__gallery gallery">
    <ul class="gallery__list">
      <li class="gallery__item">
        <a href="#" class="gallery__link">
          <img src="img\Rectangle 70.jpg" class="gallery__logo" alt="Пост">
        </a>
      </li>
```

Тем более у тебя ссылка и картинка вообще оказались детьми `gallery`
Как вариант так. Карточка сама по себе. Список - сам по себе
```html
<div class="my-post__gallery gallery">
    <ul class="gallery__list">
      <li class="gallery__item">
        <a href="#" class="gallery-item">
          <img src="img\Rectangle 70.jpg" class="gallery-item__logo" alt="Пост">
        </a>
      </li>
```
И еще, лучше задать ширину `.gallery__item` и не делать каждую строку постов отдельным списком, flex-wrap все сделает за тебя)


## CSS 

Значению 0 не требуются единицы измерения)

Линию хедера можно не ограничивать по ширине, а задать позиционирование слева и справа, тогда она будет занимать всю ширину)
```css 
.heared__line{
	width: 1448px;
	height: 1px;
	left: 0px;
}
```
Вот так
```css 
.heared__line{
  left: 0;
  right: 0
}
```

Можно же задать данный отступ родителю, дабы не копипастить)
```css
.buttons__button1{
	margin-bottom: 27px;
}
```

Css-переменный - хорошо) но несколько цветов пропустил)

Всегда помни, css - каскад, и стили накладываются в том же порядке, в котором идут в файле, поэтому модификатор должен быть всегда ниже)
```css
.navigations_icon--theme{
	color: #8E8E8E;
}
.navigations_icon{
	margin-right: 6.75px;
}
```

Немного о ховерах) не хватает плавности, `transition: .3s;` исправит)

цвет ховера - не самый удачный выбор)

`scale(2)` - как по мне, многовато, 1.1 - достаточно)

Если решил делать анимированный border, то его нужно сразу задать, чтобы потом контент не прыгал
```css
.my-cabinet__information {
	border-bottom: 2px solid transparent;
}
.my-cabinet__information:hover{
	border-bottom-color: var(--primary-accent-main);
}
```

И не забывай удалять лишнее при копирование с фигмы)
```css
.buttons__button2{
	left: 658px;
	top: 90px;
}
```
