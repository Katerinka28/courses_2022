
# Troyan Olga Instagram

Боже, милота то какая)) какой красавчик ))))

плюсик за живые фото !)

Очень хороших шрифт на замену нашла)

## HTML
К боди вопросов нет. А вот к бему хедера есть. 
`main` у тебя ведь отсутствует... А никаких элементов вне блока.
```html
<body class="body">
  <header class="main__header">
```
Тут либо у боди класс `main` 
```html
<body class="main">
  <header class="main__header">
```
либо еще одна обертка

```html
<body class="body">
  <main class='main'>
    <header class="main__header">
```
Великоватые блоки, но это придет с опытом. К примеру лого отдельно от навигации. тоже и с поиском. Навигацией может быть только кнопочки справа

Например так:

```html
<div class="logo">
  <a href="#" class='logo__link'><img class='logo__img' src="img/logo.png" alt="Instagram logo."></a>
</div>
<div class="header-search">
  <img class="header-search__icon" src="img/search.png" alt="magnifier">
  <input type="search" class="header-search__control" placeholder="Search">
</div>
<nav class="nav">
  <a href="#" class='nav__link'><img src="img/home.png" class="nav__icon" alt="home icon"></a>
  <a href="#" class='nav__link'><img src="img/direct.png" class="nav__icon" alt="direct icon"></a>
  <a href="#" class='nav__link'><img src="img/compass.png" class="nav__icon" alt="compas icon"></a>
  <a href="#" class='nav__link'><img src="img/like.png" class="nav__icon" alt="like logo"></a>
  <a href="#" class='nav__link'><img class="nav__icon" src="img/avatar.jpg" alt="mini avatar"></a>
  <div class="nav__icon">
    <img src="img/counter.png" class="nav__counter-img" alt="counter ">
    <p class="nav__counter-text">5</p>
  </div>
</nav>	
</div>
```

- Не оставляем никаких тегов без классов.

- Иконки все одинаковые - им не нужны разные классы. (`nav__icon`)

`block1` / `block2` / `block3` / `block4` - названия не совсем корректные для блоков - как только их переставишь - логика ломается. Нужно стараться придумывать классы со смыслом. 

Даже так можно, исходя из твоих классов

* `block1` - `profile-section`
* `block2` - `stories-section`
* `block3` - `holder-section`
* `block4` - `postcards-section`

```html
<li class="holder_block1"><img src="img/posts.png" class="holder__posts" alt="posts"><a href="#" class="holder__text posts">POSTS</a></li>
<li class="holder_block2"><img src="img/tagged.png" class="holder__tagged" alt="tagged"><a href="#" class="holder__text tagged">TAGGED</a></li>
```

`holder_block1` i `holder_block2` и `holder__posts` и `holder__tagged` - ничем же не отличаются) А если третий будет? Если нужно отметить активность элемента есть прекрасный класс состояния `is-active`

Карточки нужно всегда делать отдельным блоком. а не чьим то элементом.
```html
<div class="postcards">
  <img class="postcards__img" src="img/post1.jpg" alt="post">
```
Вот так
```html
<div class="postcards">
  <div class='postcard'>
    <img class="postcard__img" src="img/post1.jpg" alt="post">
  </div>
```

## CSS

и по картинкам -
`img` должен быть `display: block` и иметь ширины/высоты(даже по 100 % ) + `object-fit: cover` к примеру

его враппер - который тоже должен быть всегда -  уже регулируют его размер

стилизацию картинок нужно писать даже если ты вставила картинку с дизайна и она выглядит идеально.

Тебе собачку было не жалко? 

http://joxi.ru/4Ak7wWxs0zQybA -- твой вариант.

http://joxi.ru/Dr8XeqLHJ7N4Er -- мой с одной строчкой стилей)


```css
.holder ul
```

Теги не стилизуем . Для этого есть классы.

Цвета в переменных - отлично!


http://joxi.ru/LmGLQwNcgaMwG2 -- это все тоже одинаковое

Нужно избавряться от копипаста, а так - неплохо)

