
# Zabara Instagram

Контейнер не нужно было ограничивать шириной дизайна) то просто дизайн такой, контейнер нужно было сделать по контенту)
Можно было бы избежать огромных padding-ов)

Семантика - хорошо, но 
|  | Ссылка | Кнопка |
| ---------------- | ------ | ------ |
| За что отвечает  | Навигация, т. е. пользователь перемещается на другую часть страницы или на новую страницу. | Выполнение какой-либо функции без перехода на другую страницу — добавить в корзину, купить, отправить, проголосовать, войти и т. д. |

Исходя с таблицы, можно расставить теги верно, к примеру, Follow - это явно кнопка.

По БЭМ - старайся немного мельче делать) выделяй в блоки, то что можно переиспользовать, к примеру, карточку(превью поста) 

## Html

Где `menu` блок? Если ты начинаешь использовать его элементьы?

```html
<nav class="header__menu">
  <ul class="menu__list">
```

Вот так лучше будет)
```html
<ul class="menu">
    <li class="menu__item">
        <a href="" class="menu__link">
          <img class="menu__img" src="img/nav/home.png" alt="home">
        </a>
    </li>
```
Не забываем про модификаторы) как по мне кнопки идентичны) отличие в состоянии)
```html
<div class="post__toggle">
  <button class="post__button"><img class="post__svg" src="img/icons/VectorPost.svg" alt="button">  POSTS</button>
  <button class="post__tagged"><img class="post__svg" src="img/icons/VectorTagged.svg" alt="button">  TAGGED</button>
</div>
```


## CSS

Ховеры откровенно не симпатичные. Это конечно может субъективно. Но мое мнение такое.

Добавь хотя бы `transition`. чтобы не было так топорно.

Лучше удалай пустые блоки стилей)
```css 
.header__search {
}
```
теги не стилизуем - стилизуем классы
```css
.user__points > span:before {
    position: absolute;
    left: -12px;
    content: '';
    top: 0;
    background-color: var(--primary-dark);
    border-radius: 5px;
    font-size: 0;  
    margin-left: 2px;
    padding: 2px;
}
```

Css-переменные - отлично) шрифты - отлично) в целом по стилям - хорошо)

