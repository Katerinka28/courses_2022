
# Pavlichenko Instagram

Семантика - отлично) БЭМ - отлично)

Не забывай о модификаторах

Тут немного лишнего добавил
```html
<ul class="user-inform__number-posts-followers">
    <li class="user-inform__posts posts"><span class="posts__number">79</span> posts</li>
    <li class="user-inform__followers followers"><span class="followers__number">379</span> followers</li> 
    <li class="user-inform__folllowing following"><span class="following__number">179</span> following</li>
</ul>
```

Вот так будет лучше)

```html
<ul class="user-inform__stats">
    <li class="user-inform__stat"><b>79</b> posts</li>
    <li class="user-inform__stat"><b>379</b> followers</li> 
    <li class="user-inform__stat"><b>179</b> following</li>
</ul>
```
и стили без повышения специфичности селекторов `.user-inform__number-posts-followers > li ` css
Мы не стилизуем теги) Только классы.

## CSS

Цвета - не все в css переменных

Не забывай об точечном указании свойства
```css
    margin: 0 21px 0 0;
    margin-right: 21px;
```
С ховером ты переусердствовал, думаю, достаточно `opacity: .7` 

Значению ноль не нужны единицы измерения
```css
    padding: 0px; => padding: 0;
    margin: 0px; => margin: 0;
```

Шрифты не подключены(


Очень достойно)  Молодец!

