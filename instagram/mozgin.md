# Mozgin Instagram

Семантика хорошо) 

По БЭМ-у - неплохо, но нужно помельче делать блоки) 

`.page` - получился слишком большой. Нужно разбить еще на блоки! 

И карточки -
```html
<div class="page__gallary-block-wrapper">
  <a href="#" class="page__gallary-link">
    <img class="page__gallry-item" src="img/pic_1.jpg" alt="user-photo">
```
Их, возьми за правило, всегда делать отдельным блоком.
```html
<div class="gallery-items">
  <a href="#" class="gallery-item">
    <img class="gallery-item__img" src="img/pic_1.jpg" alt="user-photo">
  </a>

```
И `gallary` -- это `gallEry`) 

## CSS

Комментарии - отлично.

Подключение шрифтов - ок, а вот применение подкачало.

`Font-family` указываем одинаковый. Разный `font-weight`, а в `src` файлы шрифта с нужной жирности.

На элементе выбираешь нужную жирность

Некоторый свойства css являются шорткатоми (короткой записью) и объединяет в себе значение для нескольких полных свойств.
```css
.user-info__btn--suggested::after {
	border: 4px solid transparent;
	border-top: 4px solid var(--theme-secondary-color);
 }
```
Изменить можно точечно 
```css 
.user-info__btn--suggested::after {
  border-top-color: var(--theme-secondary-color);
}
```

Ховеры почти везде, обделил карточку поста, и еще не хватает анимации) `transition: 0.5s`

## JS

Возьми за правило, для js использовать классы с префиксом `.js-` , так ты тончо знаешь для чего этот класс, и если нужно будет изменить стили, то его не удалишь. 

`querySelectorAll` - забываем, и используем `getElementsByClassName`.

по квери селектору можно ходить через forEach по умолчанию(так как этот метод возвращает NodeList), а  чтобы ходить getElementsByClassName(он возвращает htmlCollection) результат нужно преобразовать в массив) и тогда все получится))

```js
let tabNav = Array.from(document.getElementsByClassName('page__gallaty-item'));
let tabContent = Array.from(document.getElementsByClassName('page__gallary-block'));
```

Data-атрибуты - хорошо. `getAttribute()` -  есть и более простой способ, используя объект dataset.
```javascript
tabName = this.dataset.tabName;
```

# Итого:
Семантика - хорошо) Бем - помельче) Стили - ок) Js - ok) Плюс за живые фотО! И за фавикон!

