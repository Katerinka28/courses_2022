Пора найти новый `normalize.css`(reset) стилей. Этому уже 5 лет. Вижу ты его дополнял, но лучше найти что-то более актуальное.

# html

Хорошо!

# css

`.bar__top .bar__mid .bar__bot` - top, mid, bot  должны быть модификаторами, и тогда `.nav__button [class*="bar__"]` не нужно. Вообще лучше не использовать сложные селекторы, они повышают [специфичность](https://developer.mozilla.org/ru/docs/Web/CSS/Specificity) стилей. 


Стили которые касаются типографии можно вынести в какой-то `caption`, и без копипаста, и универсально будет. А вот позиционирование можно оставить на данных 
```css
.box__title,
.services__title {}

.box__slogan,
.services__slogan {}
```

Background-image стараемся использовать как можно реже (в принципе все по уму, но соц сети тоже будут с админки, картинки тоже). Пробуем маленькие простенькие картинки реализовывать с помощью css (так сказать рисовать), на помощь [псевдоэлементы](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-elements), в частности `before` и `after`

Не забываем о сокращенной записи padding, margin, а еще лучше не переназначать то, что не изменяется.
```css
.page__sponsors {
  margin: 60px 0 60px 0; => margin: 60px 0; => margin-top: 60px; margin-bottom: 60px;
}
```
Вставку текста так не делаем, разве что через data-attributes. ИМХО лучше так не делать вообще!
```css
.form__btn::before {
  display: inline;
  content: "Send now";
}
```
# js 

Отказываемся от querySelector в пользу getElementByClassName. [Почитать тут](https://www.dropbox.com/scl/fi/o9lgbks1936ngmvnhlh2i/%D0%91%D1%80%D0%B0%D1%83%D0%B7%D0%B5%D1%80%D0%BD%D1%8B%D0%B9-JS.paper?dl=0&rlkey=kdjjd98rqcuvatd38hawwxksw#:uid=672203874793407432828702&h2=%D0%9A%D0%B0%D0%BA-%D0%B4%D0%BE%D1%81%D1%82%D0%B0%D1%82%D1%8C-%D1%8D%D0%BB%D0%B5%D0%BC%D0%B5%D0%BD%D1%82)

Привыкаем к порядку сразу.

Для js используем классы с префиксом `js-`, потому что названия классов для стилей, ты можешь изменить и js сломается или нужно будет изменять еще и в js (если вспомнишь). А так ты всегда знаешь, что классы `js-` используешь для js, и не удалишь из-за лишней стилизации.

Не понятен смысл оборачивания в анонимную функцию, тогда уже лучше сделать именованную функцию и её вызвать, придерживаемся функционального подхода
```javascript
(() => {
	const menuButton = document.querySelector('.nav__button');
	const menuList = document.querySelector('.nav__list');

	menuButton.addEventListener('click', () => {
		menuButton.classList.toggle('nav__button--open');
		menuList.classList.toggle('nav__list--open');
	});
})();
```

Можно обойтись модификатором на `.nav`, вместо навешивания и на кнопку, и на список.

# Итого :-)
В целом отлично.

Старайся не ограничивать высоту карточек.

В следующий раз попробуй в css задать базовою переменную, и на её основе создай список переменных для отступов, к примеру
```css
--rut-base: 10px
--space-1: var(--rut-base)
--space-2: calc(var(--rut-base) * 2)
```

Так же можно завести переменные для `font-size`-ов